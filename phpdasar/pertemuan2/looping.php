<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Looping</title>
</head>
<body>
    <h1>Berlatih Looping</h1>

    <?php 
        echo "<h3>Soal No 1 Looping I Love PHP</h3>";
      echo "<h3> Looping I Love PHP<h3>";
      echo "<h5> Looping Pertama<h5>";
      for($i=2; $i<=20; $i+=2){
        echo $i . "angka genap <br>";
      }

      echo "<h5> Looping Kedua<h5>";
      for($a=20; $a>=2; $a-=2){
        echo $a . "Angkap Genap <br>";
      }

        echo "<h3>Soal No 2 Looping Array Modulo </h3>";

        $numbers = [18, 45, 29, 61, 47, 34];
        echo "array numbers: ";
        print_r($numbers);
        echo "<br>";
        echo "Array sisa baginya adalah:"; 
        foreach($numbers as $value){
          $rest[] = $value %= 5;
        }
        print_r($rest);
    

        
        echo "<h3> Soal No 3 Looping Asociative Array </h3>";

        /* 
            Soal No 3
            Loop Associative Array
            Terdapat data items dalam bentuk array dimensi. Buatlah data tersebut ke dalam bentuk Array Asosiatif. 
            Setiap item memiliki key yaitu : id, name, price, description, source. 
            
            Output: 
            Array ( [id] => 001 [name] => Keyboard Logitek [price] => 60000 [description] => Keyboard yang mantap untuk kantoran [source] => logitek.jpeg ) 
            Array ( [id] => 002 [name] => Keyboard MSI [price] => 300000 [description] => Keyboard gaming MSI mekanik [source] => msi.jpeg ) 
            Array ( [id] => 003 [name] => Mouse Genius [price] => 50000 [description] => Mouse Genius biar lebih pinter [source] => genius.jpeg ) 
            Array ( [id] => 004 [name] => Mouse Jerry [price] => 30000 [description] => Mouse yang disukai kucing [source] => jerry.jpeg ) 

        */
        $items = [
            ['001', 'Keyboard Logitek', 60000, 'Keyboard yang mantap untuk kantoran', 'logitek.jpeg'], 
            ['002', 'Keyboard MSI', 300000, 'Keyboard gaming MSI mekanik', 'msi.jpeg'],
            ['003', 'Mouse Genius', 50000, 'Mouse Genius biar lebih pinter', 'genius.jpeg'],
            ['004', 'Mouse Jerry', 30000, 'Mouse yang disukai kucing', 'jerry.jpeg']
        ];

        foreach($items as $key => $value){
          $item = array(
            'id' => $value[0], 
            'merk' => $value[1], 
            'price' => $value[2], 
            'motto' => $value[3], 
            'img' => $value[4], 
          );
          print_r($items);
          echo "<br>";
        }
        
        // Output: 
        
        echo "<h3>Soal No 4 Asterix </h3>";
          for($j=1; $j<=5; $j++){
            for($b=1; $b<=$j; $b++){
              echo "*";
            }
            echo "<br>";
          }


        echo "Asterix: ";
        echo "<br>";       

    ?>

</body>
</html>